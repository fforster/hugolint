package hugo

import (
	"fmt"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/text"
)

type Link struct {
	Filename   string
	Href       string
	RelativeTo string
	Line       int
}

func (p *Page) Links() ([]Link, error) {
	var (
		parser = goldmark.DefaultParser()
		root   = parser.Parse(text.NewReader(p.data))
	)

	return p.links(root)
}

func (p *Page) links(node ast.Node) ([]Link, error) {
	var ret []Link

	if link, ok := node.(*ast.Link); ok {
		line, err := p.Line(positionOf(link))
		if err != nil {
			return nil, err
		}

		ret = append(ret, Link{
			Filename:   p.path,
			Href:       string(link.Destination),
			RelativeTo: path.Dir(p.path),
			Line:       line,
		})
	}

	for child := node.FirstChild(); child != nil; child = child.NextSibling() {
		links, err := p.links(child)
		if err != nil {
			return nil, err
		}

		ret = append(ret, links...)
	}

	return ret, nil
}

func positionOf(node ast.Node) int {
	switch node.Type() {
	case ast.TypeBlock:
		return node.Lines().At(0).Start
	case ast.TypeInline:
		return positionOf(node.Parent())
	case ast.TypeDocument:
		break
	}

	return -1
}

func (p *Page) BrokenLinks(webRootDir string) ([]Link, error) {
	allLinks, err := p.Links()
	if err != nil {
		return nil, err
	}

	return p.filterBroken(webRootDir, allLinks)
}

func (p *Page) filterBroken(webRootDir string, allLinks []Link) ([]Link, error) {
	var brokenLinks []Link
	for _, link := range allLinks {
		isBroken, err := p.isBroken(webRootDir, link)
		if err != nil {
			return nil, err
		}

		if isBroken {
			brokenLinks = append(brokenLinks, link)
		}
	}

	return brokenLinks, nil
}

func (p *Page) isBroken(webRootDir string, link Link) (bool, error) {
	u, err := url.Parse(link.Href)
	if err != nil {
		return false, link.anchorError(err)
	}

	// ignore remote URLs
	if u.Scheme != "" || u.Host != "" {
		return false, nil
	}

	// likely an #anchor link
	if u.Path == "" {
		return false, nil
	}

	path := u.Path
	if filepath.IsAbs(path) {
		path = filepath.Join(webRootDir, strings.TrimPrefix(path, "/"))
	} else {
		path = filepath.Join(link.RelativeTo, path)
	}

	path = strings.TrimSuffix(path, "index.html")
	path = strings.TrimSuffix(path, ".html")
	path = strings.TrimSuffix(path, "/")
	if haveIndexFile(path) {
		return false, nil
	}
	if isRegular(path + ".md") {
		return false, nil
	}

	return true, nil
}

func haveIndexFile(dir string) bool {
	indexFiles := []string{
		"_index.md",
		"index.md",
		"_index.html",
	}
	if !isDir(dir) {
		return false
	}

	for _, indexFile := range indexFiles {
		if isRegular(filepath.Join(dir, indexFile)) {
			return true
		}
	}

	return false
}

func isDir(p string) bool {
	fi, err := os.Stat(p)
	return err == nil && fi.Mode().IsDir()
}

func isRegular(p string) bool {
	fi, err := os.Stat(p)
	return err == nil && fi.Mode().IsRegular()
}

func (l Link) anchorError(err error) error {
	return AnchoredError{
		Link: l,
		Err:  err,
	}
}

type AnchoredError struct {
	Link
	Err error
}

func (e AnchoredError) Error() string {
	return fmt.Sprintf("[%s:%d] %v", e.Filename, e.Line, e.Err)
}

func (e AnchoredError) Unwrap() error {
	return e.Err
}
