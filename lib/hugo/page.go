package hugo

import (
	"fmt"
	"io"
	"os"
	"slices"
)

type Page struct {
	path   string
	data   []byte
	toLine []int
}

func LoadPage(path string) (*Page, error) {
	fh, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("os.Open(%q): %w", path, err)
	}
	defer fh.Close()

	return ReadPage(fh, path)
}

func ReadPage(r io.Reader, path string) (*Page, error) {
	data, err := io.ReadAll(r)
	if err != nil {
		return nil, fmt.Errorf("io.ReadAll: %w", err)
	}

	return &Page{
		path: path,
		data: data,
	}, nil
}

func (p *Page) Line(pos int) (int, error) {
	if pos < 0 || pos >= len(p.data) {
		return 0, fmt.Errorf("position out of range: %d", pos)
	}

	p.initToLine()

	index, _ := slices.BinarySearch(p.toLine, pos)
	return index + 1, nil
}

func (p *Page) initToLine() {
	if p.toLine != nil {
		return
	}

	for i, b := range p.data {
		if b == '\n' {
			p.toLine = append(p.toLine, i)
		}
	}
}
