package exit

import "fmt"

// Code is an exit code. It implements the Go error interface.
type Code int

// Error returns a string describing the error.
func (e Code) Error() string {
	return fmt.Sprintf("exit code %d", e)
}

// Int returns the exit code as an int.
func (e Code) Int() int {
	return int(e)
}
