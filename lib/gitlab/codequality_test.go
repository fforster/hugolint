package gitlab_test

import (
	"encoding/json"
	"testing"

	"gitlab.com/fforster/hugolint/lib/gitlab"
)

func TestFingerprint(t *testing.T) {
	cases := []struct {
		name      string
		cq1, cq2  gitlab.CodeQuality
		wantEqual bool
	}{
		{
			name: "identical",
			cq1: gitlab.CodeQuality{
				Name:        "name1",
				Description: "desc1",
				Severity:    gitlab.Info,
				Path:        "foobar",
				Line:        1,
			},
			cq2: gitlab.CodeQuality{
				Name:        "name1",
				Description: "desc1",
				Severity:    gitlab.Info,
				Path:        "foobar",
				Line:        1,
			},
			wantEqual: true,
		},
		{
			name: "name differs",
			cq1: gitlab.CodeQuality{
				Name:        "name1",
				Description: "desc1",
				Severity:    gitlab.Info,
				Path:        "foobar",
				Line:        1,
			},
			cq2: gitlab.CodeQuality{
				Name:        "name2",
				Description: "desc1",
				Severity:    gitlab.Info,
				Path:        "foobar",
				Line:        1,
			},
			wantEqual: false,
		},
		{
			name: "description differs",
			cq1: gitlab.CodeQuality{
				Name:        "name1",
				Description: "desc1",
				Severity:    gitlab.Info,
				Path:        "foobar",
				Line:        1,
			},
			cq2: gitlab.CodeQuality{
				Name:        "name1",
				Description: "desc2",
				Severity:    gitlab.Info,
				Path:        "foobar",
				Line:        1,
			},
			wantEqual: false,
		},
		{
			name: "severity differs",
			cq1: gitlab.CodeQuality{
				Name:        "name1",
				Description: "desc1",
				Severity:    gitlab.Info,
				Path:        "foobar",
				Line:        1,
			},
			cq2: gitlab.CodeQuality{
				Name:        "name1",
				Description: "desc1",
				Severity:    gitlab.Blocker,
				Path:        "foobar",
				Line:        1,
			},
			wantEqual: false,
		},
		{
			name: "path differs",
			cq1: gitlab.CodeQuality{
				Name:        "name1",
				Description: "desc1",
				Severity:    gitlab.Info,
				Path:        "path1",
				Line:        1,
			},
			cq2: gitlab.CodeQuality{
				Name:        "name1",
				Description: "desc1",
				Severity:    gitlab.Info,
				Path:        "path2",
				Line:        1,
			},
			wantEqual: false,
		},
		{
			name: "line differs",
			cq1: gitlab.CodeQuality{
				Name:        "name1",
				Description: "desc1",
				Severity:    gitlab.Info,
				Path:        "path1",
				Line:        1,
			},
			cq2: gitlab.CodeQuality{
				Name:        "name1",
				Description: "desc1",
				Severity:    gitlab.Info,
				Path:        "path1",
				Line:        2,
			},
			wantEqual: true,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			fp1, err := fingerprint(tc.cq1)
			if err != nil {
				t.Fatal(err)
			}

			fp2, err := fingerprint(tc.cq2)
			if err != nil {
				t.Fatal(err)
			}

			if gotEqual := fp1 == fp2; gotEqual != tc.wantEqual {
				t.Errorf("%v => %q, %v => %q, want equal %v", tc.cq1, fp1, tc.cq2, fp2, tc.wantEqual)
			}
		})
	}
}

func fingerprint(cq gitlab.CodeQuality) (string, error) {
	data, err := json.Marshal(cq)
	if err != nil {
		return "", err
	}

	return extractFingerprint(data)
}

func extractFingerprint(data []byte) (string, error) {
	var cq gitlab.CodeQuality

	if err := json.Unmarshal(data, &cq); err != nil {
		return "", err
	}

	return cq.Fingerprint, nil
}
