package gitlab

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
)

type CodeQuality struct {
	Description string
	Name        string
	Fingerprint string
	Severity    Severity
	Path        string
	Line        int
}

func (cq CodeQuality) MarshalJSON() ([]byte, error) {
	fingerprint, err := cq.fingerprint()
	if err != nil {
		return nil, fmt.Errorf("generating fingerprint: %w", err)
	}

	return json.Marshal(cq.wrap(fingerprint))
}

func (cq CodeQuality) fingerprint() (string, error) {
	if cq.Fingerprint != "" {
		return cq.Fingerprint, nil
	}

	hash := md5.New()

	err := json.NewEncoder(hash).Encode(cq.wrap(""))
	if err != nil {
		return "", fmt.Errorf("encoding CodeQuality for fingerprint: %w", err)
	}

	return fmt.Sprintf("%x", hash.Sum(nil)), nil
}

func (cq CodeQuality) wrap(fingerprint string) codeQuality {
	line := cq.Line
	if fingerprint == "" {
		line = 0
	}

	return codeQuality{
		Description: cq.Description,
		Name:        cq.Name,
		Fingerprint: fingerprint,
		Severity:    cq.Severity,
		Location: location{
			Path: cq.Path,
			Lines: lines{
				Begin: line,
			},
		},
	}
}

func (cq *CodeQuality) UnmarshalJSON(data []byte) error {
	var tmp codeQuality
	if err := json.Unmarshal(data, &tmp); err != nil {
		return fmt.Errorf("json.Unmarshal: %w", err)
	}

	*cq = unwrap(tmp)
	return nil
}

func unwrap(cq codeQuality) CodeQuality {
	return CodeQuality{
		Description: cq.Description,
		Name:        cq.Name,
		Fingerprint: cq.Fingerprint,
		Severity:    cq.Severity,
		Path:        cq.Location.Path,
		Line:        cq.Location.Lines.Begin,
	}
}

type Severity string

const (
	Info     Severity = "info"
	Minor    Severity = "minor"
	Major    Severity = "major"
	Critical Severity = "critical"
	Blocker  Severity = "blocker"
)

type codeQuality struct {
	Description string   `json:"description,omitempty"`
	Name        string   `json:"check_name,omitempty"`
	Fingerprint string   `json:"fingerprint,omitempty"`
	Severity    Severity `json:"severity,omitempty"`
	Location    location `json:"location,omitempty"`
}

type location struct {
	Path  string `json:"path,omitempty"`
	Lines lines  `json:"lines,omitempty"`
}

type lines struct {
	Begin int `json:"begin,omitempty"`
}
