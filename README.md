# hugolint

Hugolint implements static analysis of Markdown files specific to Hugo.

## Usage

```
hugolint linkcheck [flags] <file ...>
hugolint codequality diff <file> <file>
```

## Code quality report

`hugolint linkcheck` generates a [Code Quality](https://docs.gitlab.com/ee/ci/testing/code_quality.html) report when run with the `--format=json` flag.

If you upload the JSON output to GitLab, it will display a report of new and fixed issues in the Merge Request view. You can do so from the CI pipeline using the following configuration:

```yml
hugolint:
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  image:
    name: registry.gitlab.com/fforster/hugolint:main
  script:
    - hugolint linkcheck --format=json $(find content -type f -name '*.md') >hugolint-linkcheck.json
  artifacts:
    when: always
    expire_in: 1 month
    reports:
      codequality: hugolint-linkcheck.json
```