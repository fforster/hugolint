package linkcheck

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/fforster/hugolint/lib/gitlab"
	"gitlab.com/fforster/hugolint/lib/hugo"
)

var ErrIssuesFound = errors.New("linkcheck found issues")

func Command() *cobra.Command {
	chk := &checker{}

	cmd := &cobra.Command{
		Use:   "linkcheck",
		Short: "Check for broken links",
		Args:  cobra.MinimumNArgs(1),
		RunE:  chk.Run,
	}

	flagSet := cmd.Flags()
	flagSet.StringVar(&chk.webRoot, "webroot", "content", "Directory inside the repository that is the web root.")
	flagSet.StringVar(&chk.repoRoot, "reporoot", ".", "Directory that is the root of the repository.")
	flagSet.StringVar(&chk.format, "format", "default", "Output format. One of (default, summary, json).")

	return cmd
}

type checker struct {
	webRoot  string
	repoRoot string
	format   string
}

func (c *checker) Run(cmd *cobra.Command, args []string) error {
	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	repoRootDir := c.repoRoot
	if !filepath.IsAbs(repoRootDir) {
		repoRootDir = filepath.Join(cwd, repoRootDir)
	}

	webRootDir := c.webRoot
	if !filepath.IsAbs(webRootDir) {
		webRootDir = filepath.Join(repoRootDir, webRootDir)
	}

	log.Printf("Repository root (used for quality report): %s\n", repoRootDir)
	log.Printf("Web root (used for relative links):        %s\n", webRootDir)

	var findings []gitlab.CodeQuality
	for _, path := range args {
		f, err := checkPath(path, repoRootDir, webRootDir)

		var ae hugo.AnchoredError
		if errors.As(err, &ae) {
			findings = append(findings, newFinding(gitlab.Critical, ae.Link, repoRootDir, ae.Err.Error()))
			continue
		}
		if err != nil {
			return err
		}

		findings = append(findings, f...)
	}

	var formatter formatter
	switch c.format {
	case "default":
		formatter = &defaultFormatter{}
	case "summary":
		formatter = &summaryFormatter{}
	case "json":
		formatter = &jsonFormatter{}
	default:
		return fmt.Errorf("Unknown output format %q", c.format)
	}

	output, err := formatter.Format(findings)
	if err != nil {
		return err
	}

	fmt.Println(output)

	return formatter.Error(findings)
}

func checkPath(path, repoRootDir, webRootDir string) ([]gitlab.CodeQuality, error) {
	page, err := hugo.LoadPage(path)
	if err != nil {
		return nil, fmt.Errorf("error loading page from %q: %w", path, err)
	}

	brokenLinks, err := page.BrokenLinks(webRootDir)
	if err != nil {
		return nil, fmt.Errorf("error checking links for %q: %w", path, err)
	}

	var findings []gitlab.CodeQuality
	for _, link := range brokenLinks {
		msg := fmt.Sprintf("Link destination %q does not exist", link.Href)
		findings = append(findings, newFinding(gitlab.Major, link, repoRootDir, msg))
	}

	return findings, nil
}

func newFinding(severity gitlab.Severity, link hugo.Link, repoRootDir, msg string) gitlab.CodeQuality {
	relPath, err := filepath.Rel(repoRootDir, link.Filename)
	if err != nil {
		relPath = link.Filename
	}

	return gitlab.CodeQuality{
		Description: msg,
		Name:        "HugoLinkCheck",
		Severity:    severity,
		Path:        relPath,
		Line:        link.Line,
	}
}

type formatter interface {
	Format(findings []gitlab.CodeQuality) (string, error)
	Error(findings []gitlab.CodeQuality) error
}

type defaultFormatter struct{}

func (defaultFormatter) Format(findings []gitlab.CodeQuality) (string, error) {
	var b strings.Builder

	var prevFile string
	for _, f := range findings {
		if prevFile != f.Path {
			fmt.Fprintln(&b, f.Path)
			prevFile = f.Path
		}

		fmt.Fprintf(&b, "* %s:%d: %s\n", f.Path, f.Line, f.Description)
	}

	return b.String(), nil
}

func (defaultFormatter) Error(findings []gitlab.CodeQuality) error {
	if len(findings) > 0 {
		return ErrIssuesFound
	}

	return nil
}

type summaryFormatter struct{}

func (summaryFormatter) Format(findings []gitlab.CodeQuality) (string, error) {
	var (
		brokenLinks   int
		relativeLinks int
	)

	for _, f := range findings {
		switch f.Severity {
		case gitlab.Major:
			brokenLinks++
		case gitlab.Minor:
			relativeLinks++
		}
	}

	switch {
	case brokenLinks != 0 && relativeLinks != 0:
		return fmt.Sprintf("❌ Found %d broken links and %d relative links", brokenLinks, relativeLinks), nil
	case relativeLinks != 0:
		return fmt.Sprintf("⚠ Found %d relative links", relativeLinks), nil
	case relativeLinks == 0:
		return fmt.Sprintf("❌ Found %d broken links", brokenLinks), nil
	}

	return "✅ All links are valid", nil
}

func (summaryFormatter) Error(findings []gitlab.CodeQuality) error {
	if len(findings) > 0 {
		return ErrIssuesFound
	}

	return nil
}

type jsonFormatter struct{}

func (jsonFormatter) Format(findings []gitlab.CodeQuality) (string, error) {
	var b strings.Builder

	if err := json.NewEncoder(&b).Encode(findings); err != nil {
		return "", nil
	}

	return b.String(), nil
}

func (jsonFormatter) Error(_ []gitlab.CodeQuality) error {
	return nil
}
