package diff

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"sort"

	"github.com/spf13/cobra"
	"gitlab.com/fforster/hugolint/lib/exit"
	"gitlab.com/fforster/hugolint/lib/gitlab"
)

func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "diff",
		Short: "Show the difference between two code quality reports",
		Args:  cobra.ExactArgs(2),
		RunE:  run,
	}

	return cmd
}

func run(cmd *cobra.Command, args []string) error {
	prev, err := loadReport(args[0])
	if err != nil {
		return err
	}

	curr, err := loadReport(args[1])
	if err != nil {
		return err
	}

	fixed, broken := diff(prev, curr)

	if len(fixed) != 0 {
		fmt.Printf("Fixed (only in %q, %d issues):\n", args[0], len(fixed))
		for _, cq := range fixed {
			fmt.Println(oneline(cq, "-"))
		}
		fmt.Println()
	}

	if len(broken) != 0 {
		fmt.Printf("Newly broken (only in %q, %d issues):\n", args[1], len(broken))
		for _, cq := range broken {
			fmt.Println(oneline(cq, "+"))
		}
		fmt.Println()
		return exit.Code(1)
	}

	return nil
}

func oneline(cq gitlab.CodeQuality, prefix string) string {
	return fmt.Sprintf("%s [%s:%d]: <%s> %s", prefix, cq.Path, cq.Line, cq.Severity, cq.Description)
}

func loadReport(path string) ([]gitlab.CodeQuality, error) {
	fh, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("os.Open(%q): %w", path, err)
	}
	defer fh.Close()

	return readReport(fh)
}

func readReport(r io.Reader) ([]gitlab.CodeQuality, error) {
	var report []gitlab.CodeQuality
	if err := json.NewDecoder(r).Decode(&report); err != nil {
		return nil, fmt.Errorf("gitlab.NewDecoder(r).Decode(&report): %w", err)
	}

	return report, nil
}

func diff(cq0, cq1 []gitlab.CodeQuality) (only0, only1 []gitlab.CodeQuality) {
	have0 := make(map[string]bool, len(cq0))
	have1 := make(map[string]bool, len(cq1))

	for _, r := range cq0 {
		have0[r.Fingerprint] = true
	}

	for _, r := range cq1 {
		have1[r.Fingerprint] = true
	}

	for _, r := range cq0 {
		if !have1[r.Fingerprint] {
			only0 = append(only0, r)
		}
	}

	for _, r := range cq1 {
		if !have0[r.Fingerprint] {
			only1 = append(only1, r)
		}
	}

	sort.Sort(byPath(only0))
	sort.Sort(byPath(only1))

	return only0, only1
}

type byPath []gitlab.CodeQuality

func (a byPath) Len() int      { return len(a) }
func (a byPath) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a byPath) Less(i, j int) bool {
	if a[i].Path != a[j].Path {
		return a[i].Path < a[j].Path
	}
	return a[i].Line < a[j].Line
}
