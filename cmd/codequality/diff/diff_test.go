package diff

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/fforster/hugolint/lib/gitlab"
)

func TestDiff(t *testing.T) {
	cases := []struct {
		name         string
		cq0, cq1     []gitlab.CodeQuality
		want0, want1 []gitlab.CodeQuality
	}{
		{
			name: "equal sets",
			cq0: []gitlab.CodeQuality{
				makeCodeQuality("a"),
				makeCodeQuality("b"),
			},
			cq1: []gitlab.CodeQuality{
				makeCodeQuality("a"),
				makeCodeQuality("b"),
			},
		},
		{
			name: "order does not matter",
			cq0: []gitlab.CodeQuality{
				makeCodeQuality("a"),
				makeCodeQuality("b"),
			},
			cq1: []gitlab.CodeQuality{
				makeCodeQuality("b"),
				makeCodeQuality("a"),
			},
		},
		{
			name: "one element missing",
			cq0: []gitlab.CodeQuality{
				makeCodeQuality("a"),
				makeCodeQuality("b"),
			},
			cq1: []gitlab.CodeQuality{
				makeCodeQuality("a"),
			},
			want0: []gitlab.CodeQuality{
				makeCodeQuality("b"),
			},
		},
		{
			name: "duplicate element",
			cq0: []gitlab.CodeQuality{
				makeCodeQuality("a"),
				makeCodeQuality("a"),
				makeCodeQuality("b"),
				makeCodeQuality("a"),
			},
			cq1: []gitlab.CodeQuality{
				makeCodeQuality("a"),
				makeCodeQuality("b"),
			},
		},
		{
			name: "disjunct sets",
			cq0: []gitlab.CodeQuality{
				makeCodeQuality("a"),
				makeCodeQuality("b"),
			},
			cq1: []gitlab.CodeQuality{
				makeCodeQuality("c"),
				makeCodeQuality("d"),
			},
			want0: []gitlab.CodeQuality{
				makeCodeQuality("a"),
				makeCodeQuality("b"),
			},
			want1: []gitlab.CodeQuality{
				makeCodeQuality("c"),
				makeCodeQuality("d"),
			},
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			got0, got1 := diff(tc.cq0, tc.cq1)
			if !cmp.Equal(got0, tc.want0) || !cmp.Equal(got1, tc.want1) {
				t.Errorf("diff() = (%v, %v), want (%v, %v)", got0, got1, tc.want0, tc.want1)
			}

			// swap arguments
			got1, got0 = diff(tc.cq1, tc.cq0)
			if !cmp.Equal(got0, tc.want0) || !cmp.Equal(got1, tc.want1) {
				t.Errorf("diff() = (%v, %v), want (%v, %v)", got0, got1, tc.want0, tc.want1)
			}
		})
	}
}

func makeCodeQuality(name string) gitlab.CodeQuality {
	return gitlab.CodeQuality{
		Fingerprint: name,
		Path:        name,
	}
}
