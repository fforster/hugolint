package codequality

import (
	"github.com/spf13/cobra"
	"gitlab.com/fforster/hugolint/cmd/codequality/diff"
)

func Command() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "codequality <subcommand>",
		Short: "Various code quality utilities",
	}

	cmd.AddCommand(diff.Command())

	return cmd
}
