package missinglinks

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/fforster/hugolint/lib/hugo"
)

func Command() *cobra.Command {
	chk := &checker{}

	cmd := &cobra.Command{
		Use:   "missinglinks",
		Short: "Print broken links ordered by number of occurrences",
		Args:  cobra.MinimumNArgs(1),
		RunE:  chk.Run,
	}

	flagSet := cmd.Flags()
	flagSet.StringVar(&chk.webRoot, "webroot", "content", "Directory inside the repository that is the web root.")
	flagSet.StringVar(&chk.repoRoot, "reporoot", ".", "Directory that is the root of the repository.")
	flagSet.IntVar(&chk.num, "num", 0, "Number of links to print.")

	return cmd
}

type checker struct {
	webRoot  string
	repoRoot string
	num      int
}

func (c *checker) Run(cmd *cobra.Command, args []string) error {
	cwd, err := os.Getwd()
	if err != nil {
		return fmt.Errorf("os.Getwd(): %w", err)
	}

	repoRootDir := c.repoRoot
	if !filepath.IsAbs(repoRootDir) {
		repoRootDir = filepath.Join(cwd, repoRootDir)
	}

	webRootDir := c.webRoot
	if !filepath.IsAbs(webRootDir) {
		webRootDir = filepath.Join(repoRootDir, webRootDir)
	}

	links := map[string]int{}

	for _, path := range args {
		if err := checkPath(path, webRootDir, links); err != nil {
			log.Print(err)
		}
	}

	fmt.Print(format(links, c.num))
	return nil
}

func checkPath(path, webRootDir string, links map[string]int) error {
	page, err := hugo.LoadPage(path)
	if err != nil {
		return fmt.Errorf("error loading page from %q: %w", path, err)
	}

	brokenLinks, err := page.BrokenLinks(webRootDir)
	if err != nil {
		return fmt.Errorf("error checking links for %q: %w", path, err)
	}

	for _, link := range brokenLinks {
		u, err := url.Parse(link.Href)
		if err != nil {
			continue
		}

		links[u.Path]++
	}

	return nil
}

func format(links map[string]int, num int) string {
	if num == 0 || num > len(links) {
		num = len(links)
	}

	var linkCounts []linkCount
	for path, count := range links {
		linkCounts = append(linkCounts, linkCount{path, count})
	}

	sort.Sort(byCount(linkCounts))

	var b strings.Builder
	for i := 0; i < num; i++ {
		fmt.Fprintf(&b, "%-3d %s\n", linkCounts[i].Count, linkCounts[i].Path)
	}

	return b.String()
}

type linkCount struct {
	Path  string
	Count int
}

type byCount []linkCount

func (c byCount) Len() int      { return len(c) }
func (c byCount) Swap(i, j int) { c[i], c[j] = c[j], c[i] }
func (c byCount) Less(i, j int) bool {
	if c[i].Count == c[j].Count {
		return c[i].Path < c[j].Path
	}
	return c[i].Count > c[j].Count
}
