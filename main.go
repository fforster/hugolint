package main

import (
	"context"
	"errors"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/fforster/hugolint/cmd/codequality"
	"gitlab.com/fforster/hugolint/cmd/linkcheck"
	"gitlab.com/fforster/hugolint/cmd/missinglinks"
	"gitlab.com/fforster/hugolint/lib/exit"
)

func main() {
	ctx := context.Background()

	cmdRoot := &cobra.Command{
		Use:          "hugolint",
		Short:        "Linter for the Hugo static site generator",
		SilenceUsage: true,
	}

	cmdRoot.AddCommand(codequality.Command())
	cmdRoot.AddCommand(linkcheck.Command())
	cmdRoot.AddCommand(missinglinks.Command())

	var code exit.Code
	err := cmdRoot.ExecuteContext(ctx)
	switch {
	case errors.As(err, &code):
		os.Exit(code.Int())
	case err != nil:
		log.Print(err)
		os.Exit(1)
	}
}
