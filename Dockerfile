# Use the offical Golang image to create a build artifact.
# This is based on Debian and sets the GOPATH to /go.
FROM golang:latest as builder

# Copy local code to the container image.
WORKDIR /build
COPY *.go go.mod go.sum ./
COPY cmd ./cmd/
COPY lib ./lib/

# Build the command inside the container.
RUN go mod download
RUN CGO_ENABLED=0 go build -v -o hugolint

# Use a Docker multi-stage build to create a lean production image.
# https://docs.docker.com/develop/develop-images/multistage-build/#use-multi-stage-builds
FROM alpine
MAINTAINER  Florian Forster <fforster@gitlab.com>

# Copy the binary to the production image from the builder stage.
COPY --from=builder /build/hugolint /usr/local/bin/
